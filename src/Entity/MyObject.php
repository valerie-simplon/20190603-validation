<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyObjectRepository")
 */
class MyObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Nom de l'objet ne peut être vide")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MySubObject", inversedBy="objects", cascade={"persist"})
     * )
     */
    private $subObjects;
    
    public function __construct()
    {
        $this->subObjects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        if($this->getName()) {
            return $this->getName();
        } else {
            return 'no name';
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add subObject
     *
     * @param \App\Entity\MySubObject $subObject
     *
     * @return MyObject
     */
    public function addSubObject(\App\Entity\MySubObject $subObject)
    {
        $this->subObjects[] = $subObject;

        return $this;
    }

    /**
     * Remove subObject
     *
     * @param \App\Entity\MySubObject $subObject
     */
    public function removeSubObject(\App\Entity\MySubObject $subObject)
    {
        $this->subObjects->removeElement($subObject);
    }

    /**
     * Get subObjects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubObjects()
    {
        return $this->subObjects;
    }
}
